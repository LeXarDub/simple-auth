<?php

include __DIR__ . '/functions.php';
include __DIR__ . '/protection.php';

$pdo = getConn();
$user = getUserById($pdo, $_SESSION['uid']);

?>

<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <title>Страница пользователя</title>
    <link rel="stylesheet" href="styles.css" />
</head>
<body>
    <div class="simple-auth">
        <div class="userinfo">
            <div>Вы вошли как <strong><?=$user['username']?></strong></div>
            <a href="logout.php">Выйти</a>
        </div>
        <h1>Страница пользователя</h1>
        <div>Доступ к этой странице могут получить только авторизованные пользователи</div>
    </div>
</body>
</html>
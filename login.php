<?php

include __DIR__ . '/functions.php'; // Подключаем php файл с функциями

function login(): ?string {
    if ($_SERVER['REQUEST_METHOD'] !== 'POST') { // Если отправлен не POST запрос, выходим из функции
        return null;
    }

    $username = $_POST['username'] ?? null;
    $password = $_POST['password'] ?? null;

    if ( ! $username) {
        return 'Логин обязателен';
    }

    if ( ! $password) {
        return 'Пароль обязателен';
    }

    $pdo = getConn();
    $user = getUserByUsername($pdo, $username);

    if ( ! $user) {
        return 'Неправльный логин или пароль';
    }

    if ( ! password_verify($password, $user['password'])) {
        return 'Неправльный логин или пароль';
    }

    startSession();
    $_SESSION['uid'] = $user['id'];
    redirect('/index.php');

    return null;
}

$message = login();

?>

<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <title>Document</title>
    <link rel="stylesheet" href="styles.css" />
</head>
<body>
    <div class="simple-auth">
        <h1>Вам необходимо авторизоваться</h1>
        <form action="login.php" method="post">
            <div class="simple-auth__message"><?=$message?></div>
            <input type="text" name="username" placeholder="Логин" />
            <input type="password" name="password" placeholder="Пароль" />
            <div class="simple-auth__controls">
                <button>Войти</button>
                <a href="register.php">Зарегистрироваться</a>
            </div>
        </form>
    </div>
</body>
</html>

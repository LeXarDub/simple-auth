<?php

include __DIR__ . '/functions.php';

startSession();

$_SESSION = [];
setcookie(session_name(), '', time() - 10000); // Удаляем COOKIE PHPSESSID

session_destroy(); // Удаляем сессию

header('Location: login.php'); // Перенаправляем пользователя на страницу авторизации
